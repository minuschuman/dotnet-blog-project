# Blog Website Project

Welcome to the Blog Website project! This web application allows users to create, publish, and share blog posts on various topics. It's built using ASP.NET Core for the backend, with HTML, CSS, and JavaScript for the frontend.

## Features

- User registration and authentication
- Create, edit, and delete blog posts
- Comment on blog posts
- Search functionality to find posts by keyword
- Categorize posts by topics or tags

## Technologies Used

- ASP.NET Core
- Entity Framework Core
- HTML
- CSS
- JavaScript
- Bootstrap

## Getting Started

To run this project locally, follow these steps:

1. Install [.NET Core SDK](https://dotnet.microsoft.com/download)
2. Clone this repository to your local machine:
    ```bash
    git clone git@gitlab.com:minuschuman/dotnet-blog-project.git
    ```
3. Navigate to the project directory:
    ```bash
    cd dotnet-blog-project
    ```
4. Restore dependencies and build the project:
    ```bash
    dotnet restore
    dotnet build
    ```
5. Set up the database:
    ```bash
    dotnet ef database update
    ```
6. Run the project:
    ```bash
    dotnet run
    ```
7. Open your web browser and navigate to `http://localhost:{PORT}` to view the website.

Or, you can use docker to run the project and skip steps 4-6:
    ```bash
    docker-compose -f .docker/docker-compose-build.yml up --build
    ```

## Project Structure

The project structure is organized as follows:

- `src/`: Contains the backend code written in C# using ASP.NET Core.
- `wwwroot/`: Contains static files such as CSS, JavaScript, and images for the frontend.
- `Views/`: Contains Razor views for rendering HTML content.
- `Models/`: Defines data models and entities used in the application.
- `Controllers/`: Contains controllers to handle HTTP requests and responses.

## How to Contribute

We welcome contributions from the community! If you'd like to contribute to this project, please follow these guidelines:

1. Fork the repository and create a new branch for your feature or bug fix.
2. Make your changes and ensure that all tests pass.
3. Submit a pull request with a clear description of your changes and the problem they solve.
4. Be responsive to feedback and iterate on your contributions as needed.

## License

This project is licensed under the MIT License. See the [LICENSE](LICENSE) file for details.

## Acknowledgments

Special thanks to the contributors and supporters of this project for their valuable contributions and feedback.
